const express = require("express");

const app = express();
const port = 3004

// app.use(express.static("public"))

const path = require('path')
app.use('/static', express.static(path.join(__dirname, 'public')))

app.use(express.json())


let posts = [{id:"1", title:"MERN", body:"I'm learning MERN"}]

app.get('/', (req,res) =>{
    res.status(200).json({message:"Success",posts:posts})
})

app.get('/:id', (req,res) =>{
    const {id} = req.params;
    const post = posts.find((post) => post.id ===id);
    if(!post){
       return  res.status(404).json({message:"Failure"})
    }
    res.status(200).json({message:"Success",post:post})
})

app.post('/', (req,res) =>{
    const {id,title,body} = req.body;
    posts.push({id,title,body});

    res.status(200).json( {message:"Success", post: {id,title,body} });
})

app.put('/:id', (req,res) =>{
    const {id} = req.params;
    const {title, body} = req.body;

    const index = posts.findIndex((post) => post.id === id);
    if(index <0){
        return res.status(404).json({message:"Failure"})
    }
    posts[index] = {id,title,body}
    res.status(200).send({message:"Success", post: {id,title,body}})
})

app.patch('/:id', (req,res) =>{
    const {id} = req.params;
    const {title} = req.body;

    const index = posts.findIndex((post) => post.id === id);
    if(index <0){
        return res.status(404).json({message:"Failure"})
    }
    posts[index] = {...posts[index], title}
    res.status(200).send({message:"Success", post: posts[index]})
})

app.delete('/:id', (req,res) =>{
    const {id} = req.params;

    posts = posts.filter((post) =>post.id !== id);
    res.status(200).json({message:"Success"})
})

app.listen(port, () =>{
    console.log(`Server is running at ${port}`)
})