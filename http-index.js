const http = require('http');
const crypto = require('crypto');

const hostname = '127.0.0.1';
const port = 3002;

const server = http.createServer((req,res)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    const hash = crypto.createHash('sha256').update("Hello World!").digest('hex');
    res.end(hash);

    //res.end("hello world im from node...")
})

server.listen(port, hostname, () =>{
    console.log(`server is running at http://${hostname}/${port}`)
})